const express = require('express');
const bodyParser = require('body-parser');
const schedule = require('node-schedule');

const scraper = require('./handler/scraper');

var app = express();

//set port
app.set('port', (process.env.PORT || 5000));

//body-parser
app.use(bodyParser.json());

app.use(require('./handler/event_handler'));

app.get('/', function(req, res){
  res.sendFile(__dirname + '/index.html');
});

var server = app.listen(app.get('port'), function(){
  console.log('Node app is running on port', app.get('port'));
});

schedule.scheduleJob('*/10 * * * *', function(){
  console.log('Update dimulai');
  scraper.get_info();
  console.log('Update selesai');
});
