const express = require('express');
const line = require('@line/bot-sdk');
const MongoClient = require('mongodb').MongoClient;
const assert = require('assert');

const router = express.Router();

const send_message = require('./send_message');
const scraper = require('./scraper');

const config = {
  channelAccessToken: '**',
  channelSecret: '**'
};
const client = new line.Client(config);

router.post('/webhook', function(req, res, next){
  var events = req.body.events;
  for(var i = 0; i < events.length; i++){
      var event = events[i];
      event_handler(event, res);
  }
});

module.exports = router;

//event handler
function event_handler(event, res) {
  console.log(event);
  var source = event.source;
  if(event.type === 'message' && source.type === 'user'){
    if(event.message.type === 'text'){
      var text = event.message.text;

      if(text === '/info' && source.type === 'user'){
        get_info(event.replyToken, res);
      }else if(text === '/testpush' && source.userId === 'Ued0a8f5d96cdcb13fe1998fad72b21db'){
        var message = {type: 'text', text: 'Test push berhasil'};
        send_message.testPushMessage(source.userId, message, res);
      }else if(text === '/testupdate' && source.userId === 'Ued0a8f5d96cdcb13fe1998fad72b21db'){
        var message = {type: 'text', text: 'Test udpate berhasil'};
        scraper.get_info();
        send_message.testPushMessage(source.userId, message, res);
      }else {
        res.json(Promise.resolve(null));
      }
    }else {
      res.json(Promise.resolve(null)); //pesan bukan teks
    }
  }else if(event.type === 'follow'){
    var message = {type: 'text', text: 'Terimakasih telah menambahkan bot infoAkademik{jteti}.'};
    send_message.replyMessage(event.replyToken, message, res);
    add_userId(source.userId);
  }else if(event.type === 'unfollow'){
    delete_userId(source.userId);
  }else if(event.type === 'join'){
    var message = {type: 'text', text: 'Terimakasih telah menambahkan bot infoAkademik{jteti}.'};
    send_message.replyMessage(event.replyToken, message, res);
    var groupId;
    if(source.type === 'group'){
      groupId = source.groupId;
    }else{
      groupId = source.roomId;
    }

    add_groupId(groupId);
  }else if(event.type === 'leave'){
    var groupId;
    if(source.type === 'group'){
      groupId = source.groupId;
    }else{
      groupId = source.roomId;
    }

    delete_groupId(groupId);
  }
  else {
    res.json(Promise.resolve(null));
  }
}

function get_info(replyToken, res){
  var url = '*';
  MongoClient.connect(url, function(err, db){
    assert.equal(null, err);
    console.log("Connected successfully to server");

    db.collection('db_info').find().toArray(function(err, info_db){
      var pesan = '';
      for(var i = 0; i < info_db.length; i++){
        var info = info_db[i].tanggal + '\n' + info_db[i].judul;
        pesan = pesan + info;
        pesan = pesan + '\n---------------------------------------\n';
      }
      pesan = pesan + '\nSelengkapnya di http://sarjana.jteti.ugm.ac.id/akademik';

      var message = {type: 'text', text: pesan};
      send_message.replyMessage(replyToken, message, res);

      db.close();
    });
  });
}

function add_userId(userId){
  var url = '*';
  MongoClient.connect(url, function(err, db){
    assert.equal(null, err);
    console.log("Connected successfully to server");

    db.collection('db_userid').insertOne({userId: userId}, function(err, result){
      if (err) {
        return;
      }
      console.log('add id_user succes');
      db.close();
    });
  });
}

function delete_userId(userId){
  var url = '*';
  MongoClient.connect(url, function(err, db){
    assert.equal(null, err);
    console.log("Connected successfully to server");

    db.collection('db_userid').deleteOne({userId: userId}, function(err, result){
      if (err) {
        return;
      }
      console.log('delete id_user succes');
      db.close();
    });
  });
}

function add_groupId(groupId){
  var url = '*';
  MongoClient.connect(url, function(err, db){
    assert.equal(null, err);
    console.log("Connected successfully to server");

    db.collection('db_groupid').insertOne({groupId: groupId}, function(err, result){
      if (err) {
        return;
      }
      console.log('add groupId succes');
      db.close();
    });
  });
}

function delete_groupId(groupId){
  var url = '*';
  MongoClient.connect(url, function(err, db){
    assert.equal(null, err);
    console.log("Connected successfully to server");

    db.collection('db_groupid').deleteOne({groupId: groupId}, function(err, result){
      if (err) {
        return;
      }
      console.log('delete groupId succes');
      db.close();
    });
  });
}
