const line = require('@line/bot-sdk');

const config = {
  channelAccessToken: '**',
  channelSecret: '**'
};
const client = new line.Client(config);

module.exports.replyMessage = function(replyToken, message, res){
  client.replyMessage(replyToken, message).then(function(result){
    res.json(result);
  }).catch(function(err){
    console.log(err);
  });
};

module.exports.pushMessage = function(userId, message){
  client.pushMessage(userId, message).then(function(result){
    console.log('Info terkirim');
  });
};

module.exports.testPushMessage = function(userId, message, res){
  client.pushMessage(userId, message).then(function(result){
    res.json(result);
  }).catch(function(err){
    console.log(err);
  });
};
