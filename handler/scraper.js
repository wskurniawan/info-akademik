var request = require('request');
var cheerio = require('cheerio');
var MongoClient = require('mongodb').MongoClient;
var assert = require('assert');
var send_message = require('./send_message');

module.exports.get_info = function(){
  var url = 'http://sarjana.jteti.ugm.ac.id/akademik';
  var infos = [];

  request(url, function(error, response, html){
    if(error){
      console.error(error);
      res.send({
        events: req.body.events,
        status: error
      });
      return;
    }

    var documents = cheerio.load(html);

    //mendapatkan informasi dari web
    documents('tr').each(function(i, elem){
      var info = {tanggal, judul, detail, deskripsi};
      if(i != 0){
        var tanggal = documents(this).children().first().text();
        var judul = documents(this).children().eq(2).children().eq(2).text();
        var deskripsi = documents(this).children().eq(2).children().eq(5).text();
        var detail = documents(this).children().eq(2).children().eq(6).text();

        info.tanggal = tanggal;
        info.judul = judul;
        if(deskripsi != ''){
          info.deskripsi = deskripsi;
        }else{
          info.deskripsi = null;
        }
        if(detail != ''){
          info.detail = detail;
        }else {
          info.detail = null;
        }

        infos[i - 1] = info;
      }

      if(i === 5){
        return false;
      }
    });

    update_db(infos);
  });
};

function update_db(infos){
  var url = '**';
  MongoClient.connect(url, function(err, db){
    assert.equal(null, err);
    console.log("Connected successfully to server");
    db.collection('db_info').find().toArray(function(err, info_db){
      console.log(info_db);
      if(info_db.length === 0){
        for(var i = 0; i <= infos.length; i++){
          var insert = {
            tanggal: infos[i].tanggal,
            judul: infos[i].judul,
            deskripsi: infos[i].deskripsi,
            detail: infos[i].detail
          };

          db.collection('db_info').insertOne(insert, function(err, result){
            if(err){
              console.log(err.message);
              db.close();
              return;
            }

            var info = result.ops[0];
          });

          if(i === infos.length - 1){
            console.log('Operasi insert sukses');
            db.close();
          }
        }
      }else {
        var jml_update = 0;

        //cek jumlah info baru
        for(var i = 0; i < infos.length; i++){
          if(info_db[0].judul != infos[i].judul){
            jml_update += 1;
          }else {
            break;
          }
        }

        //melakukan update dan push message
        if(jml_update != 0){
          console.log('Info baru: ' + jml_update);
          for(var i = 0; i < info_db.length; i++){
            db.collection('db_info').updateOne(info_db[i], infos[i], function(err, result){
              console.log('update successfully');

              if(i === info_db.length - 1){
                db.close();
              }
            });
          }

          for(var i = jml_update - 1; i >= 0; i--){
            var pesan = infos[i].tanggal + '\n' + infos[i].judul;
            if(infos[i].deskripsi != null){
              pesan = pesan + '\n' + infos[i].deskripsi;
            }
            if(infos[i].detail != null){
              pesan = pesan + '\n' + infos[i].detail;
            }
            if(i === 0){
              pesan = pesan + '\n\nSelengkapnya di http://sarjana.jteti.ugm.ac.id/akademik';
            }
            var message = {type: 'text', text: pesan};
            pushToUser(message);
            pushToGroup(message);
          }
        }else {
          db.close();
          console.log('tidak ada update baru');
        }
      }
    });
  });

}

function pushToUser(message){
  var url = '**';
  MongoClient.connect(url, function(err, db){
    assert.equal(null, err);
    console.log("Connected successfully to server");
    db.collection('db_userid').find().toArray(function(err, users){
      if(users.length != 0){
        for(var i = 0; i < users.length; i++){
          send_message.pushMessage(users[i].userId, message);
          if(i === users.length - 1){
            db.close();
          }
        }
      }else {
        db.close();
      }
    });
  });
}

function pushToGroup(message){
  var url = '**';
  MongoClient.connect(url, function(err, db){
    assert.equal(null, err);
    console.log("Connected successfully to server");
    db.collection('db_groupid').find().toArray(function(err, groups){
      if(groups.length != 0){
        for(var i = 0; i < groups.length; i++){
          send_message.pushMessage(groups[i].groupId, message);
          if(i === groups.length - 1){
            db.close();
          }
        }
      }else {
        db.close();
      }
    });
  });
}
